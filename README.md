
<div align="center">

<img src="https://gitlab.com/beraldoleal/insights4ci/-/raw/main/ui/src/assets/insights4ci-logo.png" alt="Insights4CI" width="250">

![Stability](https://img.shields.io/badge/stability-experimental-red.svg)
![Contributions welcome](https://img.shields.io/badge/contributions-welcome-orange.svg)
![Python](https://img.shields.io/badge/Python-v3.6+-blue.svg)
![VueJS3](https://img.shields.io/badge/VueJS-v3-brightgreen.svg)
![License](https://img.shields.io/badge/license-GPLv2-blue.svg)

Insights4CI is **an experimental project** intended to provide project and
runners maintainers, developers, and QE teams with insights on CI/CD pipelines,
helping them better understand their test pipelines.

[Installation](#installation) •
[How to Contribute](#contributing) •
</div>

![Work in Progress Interface](/assets/print.png "Test result page (WiP)")

## Installing

For convenience, this project has a `docker-compose.yml` file so you can use
`docker-compose` to bootstrap all services (db, api_server, and web client). So
in case you feel comfortable with Docker containers, just run the following
command and wait a few minutes for the first pulling:

    $ docker-compose up

However, if you don't like to use containers here, you can set up each service
manually.  Please visit `server/README.md` and `ui/README.md` for details.

## Populating

Fow now, a background task is started after a project is created. It will fetch
test results from the backend.

If your backend is not supported or you would like to populate data manually,
see the `utils/deprecate/populate.py` script.

## Debugging background tasks

You can query backgroud task status connecting to the Flower UI. Just point
your browser to [http://localhost:5555](http://localhost:5555).

## Contributing

This project has just started, so there is no better time for you to
collaborate. 

Have a look through existing
[Issues](https://gitlab.com/beraldoleal/insights4ci/-/issues) and [Merge
Requests](https://gitlab.com/beraldoleal/insights4ci/-/merge_requests) that you
could help with. If you'd like to request a feature or report a bug, please
create a [new issue](https://gitlab.com/beraldoleal/insights4ci/-/issues/new).

## License

This project is licensed under the terms of the GPL Open Source license (v2)
and is available for free.
