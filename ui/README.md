# Insights4CI - Client side

This directory is holding a VueJS (v3) project, intended to provide a dashboard
to analyze data from CI's pipelines.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
