import { createRouter, createWebHistory } from 'vue-router'
// import Home from '../views/Home.vue'

const routes = [
  { path: '/', name: 'Home', redirect: '/projects' },
  { path: '/projects', name: 'Projects', component: () => import('../views/Projects.vue') },
  { path: '/projects/:id', name: 'ProjectDetails', component: () => import('../views/ProjectDetails.vue') },
  { path: '/about', name: 'About', component: () => import('../views/About.vue') },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
