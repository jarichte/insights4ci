import re

from copy import copy
from datetime import timedelta, date
from functools import lru_cache, cached_property
from typing import List
from uuid import uuid4

from sqlalchemy import (Boolean, Column, DateTime, Float,
                        ForeignKey, Integer, String)
from sqlalchemy.orm import Session, relationship
from sqlalchemy.exc import IntegrityError

from junitparser import Failure, Skipped, Error, JUnitXml

from insights4ci import schemas
from insights4ci.database import Base
from insights4ci.backends import gitlab


class I4CException(Exception):
    pass


class I4CAlreadyExists(I4CException):
    pass


class I4CIntegrityError(I4CException):
    pass


class BaseModel(Base):
    __abstract__ = True

    @classmethod
    def get(cls, db: Session, id: int):
        return db.query(cls).filter(cls.id == id).first()

    @classmethod
    def get_all(cls, db: Session, skip: int = 0, limit: int = 100):
        return db.query(cls).offset(skip).limit(limit).all()

    def save_to_session(self, db: Session):
        try:
            db.add(self)
            db.commit()
            db.refresh(self)
            return self
        except Exception:
            db.rollback()
            raise


class Runner(BaseModel):
    __tablename__ = "runners"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String(255), unique=True, index=True)
    owner = Column(String(255))
    external_id = Column(String(255))

    description = Column(String(255))
    architecture = Column(String(255))
    platform = Column(String(255))
    last_seen = Column(DateTime)

    jobs = relationship("Job", back_populates="runner")

    @classmethod
    def get_runner_by_id(cls, db: Session, id: int):
        return db.query(cls).filter(cls.id == id).first()

    @classmethod
    def get_runner_by_name(cls, db: Session, name: str):
        return db.query(cls).filter(cls.name == name).first()

    @classmethod
    def get_by_schema_or_create(cls,
                                db: Session,
                                schema: schemas.RunnerCreate):
        result = cls.get_runner_by_name(db, schema.name)
        if not result:
            result = cls.create_from_schema(db, schema)
        return result

    @classmethod
    def create_from_schema(cls,
                           db: Session,
                           schema: schemas.RunnerCreate):
        obj = cls(**schema.dict())
        return obj.save_to_session(db)

    @classmethod
    def create_from_dict(cls,
                         db: Session,
                         data: dict):
        schema = schemas.RunnerCreate(**data)
        return cls.create_from_schema(db, schema)


class Project(BaseModel):
    __tablename__ = "projects"

    id = Column(Integer, primary_key=True, index=True)

    name = Column(String(255), unique=True, index=True)

    description = Column(String(255))
    repository_url = Column(String(255))

    pipelines = relationship("Pipeline", back_populates="project",
                             order_by="Pipeline.created_at.desc()")
    tests = relationship("Test", back_populates="project")

    @classmethod
    def get_project_by_id(cls, db: Session, id: int):
        return db.query(cls).filter(cls.id == id).first()

    @classmethod
    def get_project_by_name(cls, db: Session, name: str):
        return db.query(cls).filter(cls.name == name).first()

    @classmethod
    def create_from_schema(cls, db: Session, schema: schemas.ProjectCreate):
        obj = cls(**schema.dict())
        try:
            return obj.save_to_session(db)
        except IntegrityError as ex:
            raise I4CIntegrityError(f"Failed to create project: {ex}") from ex

    @classmethod
    def create_from_dict(cls, db: Session, data: dict):
        schema = schemas.ProjectCreate(**data)
        return cls.create_from_schema(db, schema)

    @cached_property
    def tests_length(self):
        return len(self.tests)

    @cached_property
    def latest_pipeline(self):
        try:
            return self.pipelines[0]
        except IndexError:
            return None

    @property
    def jobs(self):
        result = []
        for pipeline in self.pipelines:
            for job in pipeline.jobs:
                result.append(job)
        return result

    @property
    def backend(self):
        # Very basic and naive approach
        if 'gitlab' in self.repository_url:
            return gitlab
        else:
            return None

    def get_pipeline_by_id(self, id: int):
        result = list(filter(lambda x: x.id == id, self.pipelines))
        if result:
            return result[0]
        else:
            return None

    def get_job_by_id(self, db: Session, id: int):
        job = Job.get_by_id(db, id)
        assert job.pipeline.project == self
        return job

    def get_pipelines(self,
                      dt_from: date = date.today(),
                      dt_to: date = date.today() - timedelta(days=7),
                      in_groups: List[int] = []):
        result = []
        # TODO: This is an inplace hack that could be avoided.
        for pipeline in self.pipelines:
            pipeline.stats_group_filters = in_groups
            if pipeline.created_at.date() < dt_from or \
                    pipeline.created_at.date() > dt_to:
                continue
            result.append(pipeline)
        return result

    def get_test_summary(self,
                         dt_from: date = date.today(),
                         dt_to: date = date.today() - timedelta(days=7),
                         in_groups: List[int] = []):
        stats = {'success': 0,
                 'failed': 0,
                 'error': 0,
                 'skipped': 0,
                 'total': 0}
        tests = {}
        for test in self.tests:
            if not TestGroup.match(test.name, in_groups):
                continue
            for result in test.test_results:
                if result.job.created_at.date() < dt_from or \
                        result.job.created_at.date() > dt_to:
                    continue
                try:
                    tests[test.name]['stats'][result.status] += 1
                    tests[test.name]['stats']['total'] += 1
                except KeyError:
                    tests[test.name] = {'stats': copy(stats),
                                        'test_id': test.id}
                    tests[test.name]['stats'][result.status] += 1
                    tests[test.name]['stats']['total'] += 1
        return tests


class TestGroup:
    FILTERS = {'1': {'name': 'ARM SMMU',
                     'files': ['tests/avocado/smmu.py']},
               '2': {'name': 'Record/Replay',
                     'files': ['tests/avocado/replay_*.py', 'tests/avocado/reverse_debugging.py']},
               '3': {'name': 'S390 General Architecture Support',
                     'files': ['tests/migration/s390x/']}}

    @classmethod
    def match(cls,
              test_name: str,
              group_filters: List[int] = []):
        if not group_filters:
            return True

        for group_id in group_filters:
            for file_pattern in cls.FILTERS[group_id]['files']:
                # Small fix until we remove the test prefix counter
                real_name = ''.join(test_name.split('-')[1:])
                filename = ''.join(real_name.split(':')[0])
                if re.match(file_pattern, filename):
                    return True
        return False


class Pipeline(BaseModel):
    __tablename__ = "pipelines"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    external_id = Column(String(255), index=True)

    status = Column(String(64))
    sha = Column(String(255))
    ref = Column(String(255))
    source = Column(String(64))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    external_url = Column(String(255))

    project_id = Column(Integer, ForeignKey("projects.id"), primary_key=True)
    project = relationship("Project", back_populates="pipelines")
    jobs = relationship("Job", back_populates="pipeline")

    stats_group_filters = []

    @property
    def tests_stats(self):
        stats = {'failed': 0,
                 'success': 0,
                 'skipped': 0,
                 'error': 0}

        for job in self.jobs:
            for result in job.test_results:
                if TestGroup.match(result.test.name,
                                   self.stats_group_filters):
                    stats[result.status] += 1

        return stats

    @classmethod
    def create_from_schema(cls, db: Session,
                           schema: schemas.PipelineCreate,
                           project_id: int):
        obj = cls(**schema.dict(), project_id=project_id)
        try:
            return obj.save_to_session(db)
        except IntegrityError as ex:
            raise I4CIntegrityError(f"Failed to create pipeline: {ex}") from ex

    @classmethod
    def create_from_dict(cls, db: Session,
                         data: dict,
                         project_id: int):
        schema = schemas.PipelineCreate(**data)
        return cls.create_from_schema(db, schema, project_id)


class Job(BaseModel):
    __tablename__ = "jobs"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)

    external_id = Column(String(255), index=True)

    name = Column(String(255))
    status = Column(String(255))
    stage = Column(String(255))
    ref = Column(String(255))
    allow_failure = Column(Boolean, default=False)
    external_url = Column(String(255))
    commit = Column(String(255))
    created_at = Column(DateTime)
    started_at = Column(DateTime)
    finished_at = Column(DateTime)

    runner_id = Column(Integer, ForeignKey("runners.id"), primary_key=True)
    runner = relationship("Runner", back_populates="jobs")

    pipeline_id = Column(Integer, ForeignKey("pipelines.id"), primary_key=True)
    pipeline = relationship("Pipeline", back_populates="jobs")

    test_results = relationship("TestResult", back_populates="job")

    @cached_property
    def tests_length(self):
        return len(self.test_results)

    @classmethod
    @lru_cache
    def get_by_external_id(cls, db: Session, external_id: str):
        return db.query(cls).filter(cls.external_id == external_id).first()

    @classmethod
    def get_by_id(cls, db: Session, id: int):
        return db.query(cls).filter(cls.id == id).first()

    @classmethod
    def create_from_schema(cls, db: Session,
                           schema: schemas.JobCreate,
                           pipeline_id: int):
        # Creates Runner
        if schema.runner:
            runner = Runner.get_by_schema_or_create(db, schema.runner)
        else:
            # To avoid orphans in database, lets create a fake runner
            data = {'name': 'unknown',
                    'owner': 'insights4ci',
                    'external_id': 0,
                    'description': 'This job has no runner attached.'}
            runner_schema = schemas.RunnerCreate(**data)
            runner = Runner.get_by_schema_or_create(db, runner_schema)

        args = schema.dict()
        args.pop('runner')
        args.pop('test_results')

        # Creates Job
        args['runner_id'] = runner.id
        obj = cls(**args, pipeline_id=pipeline_id)
        try:
            job = obj.save_to_session(db)
        except IntegrityError as ex:
            raise I4CIntegrityError(f"Failed to create job: {ex}") from ex

        # Let's split test and test results and create them.
        for test_result in schema.test_results:
            test = Test.get_by_name_or_create(db,
                                              test_result.name,
                                              test_result.class_name,
                                              job.pipeline.project.id)
            data = {'status': test_result.status,
                    'execution_time': test_result.execution_time}
            TestResult.create_from_dict(db, data, test.id, job.id)
        return job

    @classmethod
    def create_from_dict(cls, db: Session,
                         data: dict,
                         pipeline_id: int):
        schema = schemas.JobCreate(**data)
        return cls.create_from_schema(db, schema, pipeline_id)

    @classmethod
    def create_from_xunit(cls, db: Session, xunit_data, pipeline):
        """Create jobs in a pipeline from a XUnit data."""

        # This library sucks. It is not so smart, so we are assuming for now,
        # one job per XML on a root element (testsuite).
        # Also, for now we are creating a fake external_id, and an unknown
        # status since Avocado is not exporting this on XML.
        xml = JUnitXml.fromstring(xunit_data)
        job = cls.create_from_dict(db,
                                   {'name': xml.name,
                                    'created_at': xml.timestamp,
                                    'external_id': str(uuid4()),
                                    'status': 'unknown'},
                                   pipeline.id)
        for case in xml:
            test = Test.get_by_name_or_create(db, case.name, case.classname,
                                              pipeline.project.id)
            result = "PASS"
            if case.result:
                if isinstance(case.result[0], Failure):
                    result = "FAIL"
                elif isinstance(case.result[0], Skipped):
                    result = "SKIP"
                elif isinstance(case.result[0], Error):
                    result = "ERROR"

            data = {'status': result,
                    'execution_time': case.time}
            TestResult.create_from_dict(db, data, test.id, job.id)


class Test(BaseModel):
    __tablename__ = "tests"

    # Multiple primary_keys it will create a PrimaryKeyContraint. Also, since
    # it is a composite, autoincrement is not default here.
    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String(255), primary_key=True)
    class_name = Column(String(255), primary_key=True, nullable=True)
    project_id = Column(Integer, ForeignKey("projects.id"), primary_key=True)

    project = relationship("Project", back_populates="tests")
    test_results = relationship("TestResult", back_populates="test")

    # UniqueConstraint('id', 'name', 'class_name', name='test_id_1')

    @classmethod
    def get_by_name_or_create(cls, db: Session, name: str, class_name: str,
                              project_id: int):
        result = db.query(cls).filter(
                cls.name == name).filter(
                cls.class_name == class_name).filter(
                cls.project_id == project_id).first()
        if not result:
            result = cls(name=name,
                         class_name=class_name,
                         project_id=project_id)
            result.save_to_session(db)
        return result


class TestResult(BaseModel):
    __tablename__ = "test_results"

    id = Column(Integer, primary_key=True, index=True)

    status = Column(String(255))
    execution_time = Column(Float)

    test_id = Column(Integer, ForeignKey("tests.id"))
    test = relationship("Test", back_populates="test_results")

    job_id = Column(Integer, ForeignKey("jobs.id"))
    job = relationship("Job", back_populates="test_results")

    @classmethod
    def create_from_schema(cls, db: Session,
                           schema: schemas.TestResultAloneCreate,
                           test_id: int,
                           job_id: int):
        obj = cls(**schema.dict(), test_id=test_id, job_id=job_id)
        return obj.save_to_session(db)

    @classmethod
    def create_from_dict(cls, db: Session,
                         data: dict,
                         test_id: int,
                         job_id: int):
        schema = schemas.TestResultAloneCreate(**data)
        return cls.create_from_schema(db, schema, test_id, job_id)
