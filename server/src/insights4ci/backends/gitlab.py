#!/usr/bin/env python3
from functools import cached_property

import logging

import gitlab

from insights4ci.backends.common import (GenericProject, GenericPipeline,
                                         GenericJob, GenericTestResult,
                                         GenericRunner)


LOG = logging.getLogger('insights4ci.backends.gitlab')

GITLAB_ENDPOINT = 'https://gitlab.com'
GITLAB = gitlab.Gitlab(GITLAB_ENDPOINT)


class Project(GenericProject):
    @property
    def name(self):
        return self.project.name

    @property
    def description(self):
        return self.project.description

    @property
    def repository_url(self):
        return self.project.http_url_to_repo

    @classmethod
    def from_namespace(cls, namespace):
        project = GITLAB.projects.get(namespace)
        return cls(project.id, project._attrs)

    @cached_property
    def project(self):
        return GITLAB.projects.get(self.external_id)

    @property
    def pipelines(self):
        LOG.info(f"Fetching pipelines for project {self.name}...")
        if not self._pipelines:
            result = []
            # Let's get only the latest 20 pipelines for now
            for pipeline in self.project.pipelines.list(per_page=20,
                                                        page=1,
                                                        as_list=False):
                status = pipeline.attributes.get('status')
                if status in ['running', 'skipped', 'canceled']:
                    continue
                result.append(Pipeline(pipeline.attributes, self))
            self._pipelines = result
        return self._pipelines


class Pipeline(GenericPipeline):
    @cached_property
    def _raw_pipeline(self):
        return self.project.project.pipelines.get(self.external_id)

    @property
    def external_id(self):
        return self.data.get('id')

    @property
    def status(self):
        return self.data.get('status')

    @property
    def sha(self):
        return self.data.get('sha')

    @property
    def ref(self):
        return self.data.get('ref')

    @property
    def source(self):
        return 'gitlab'

    @property
    def created_at(self):
        return self.data.get('created_at')

    @property
    def updated_at(self):
        return self.data.get('updated_at')

    @property
    def external_url(self):
        return self.data.get('web_url')

    @property
    def jobs(self):
        LOG.info(f"Fetching jobs for pipeline {self.external_id}...")
        if not self._jobs:
            self._jobs = [Job(job.attributes,
                              Runner(job.runner) if job.runner else None,
                              self)
                          for job in self._raw_pipeline.jobs.list(per_page=100,
                                                                  as_list=False)]
        return self._jobs

    @cached_property
    def report(self):
        return self._raw_pipeline.test_report.get()


class Job(GenericJob):
    @property
    def external_id(self):
        return self.data.get('id')

    @property
    def name(self):
        return self.data.get('name')

    @property
    def status(self):
        return self.data.get('status')

    @property
    def stage(self):
        return self.data.get('stage')

    @property
    def external_url(self):
        return self.data.get('web_url')

    @property
    def started_at(self):
        return self.data.get('started_at')

    @property
    def created_at(self):
        return self.data.get('created_at')

    @property
    def finished_at(self):
        return self.data.get('finished_at')

    @cached_property
    def test_results(self):
        LOG.info(f"Fetching test runs for job {self.external_id}...")
        result = []
        for suite in self.pipeline.report.test_suites:
            if suite['name'] == self.name:
                result = [TestResult(test_case, self)
                          for test_case in suite['test_cases']]
                break
        return result


class Runner(GenericRunner):
    @property
    def external_id(self):
        return self.data.get('id')

    @property
    def name(self):
        return self.data.get('name')

    @property
    def description(self):
        return self.data.get('description')

    @property
    def owner(self):
        return "gitlab"

    @property
    def architecture(self):
        # TODO: Fix this for custom runners
        return "x86_64"

    @property
    def platform(self):
        # TODO: Fix this for custom runners
        return "Linux"


# class Test:
# So far, there is no need to create a Test object here.

class TestResult(GenericTestResult):
    @property
    def name(self):
        return self.data.get('name')

    @property
    def class_name(self):
        return self.data.get('classname')

    @property
    def status(self):
        return self.data.get('status')

    @property
    def execution_time(self):
        return self.data.get('execution_time')
