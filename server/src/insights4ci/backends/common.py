import json

from abc import abstractmethod
from functools import cached_property
from uuid import uuid4


class GenericObj:
    """Generic remote object when representing back end data."""

    def __init__(self, data):
        self.data = data

    def __hash__(self):
        return hash(self.as_json())

    def as_dict(self):
        pass

    def as_json(self, sort_keys=True, indent=4):
        return json.dumps(self.as_dict(), sort_keys=sort_keys, indent=indent)


class GenericProject(GenericObj):
    def __init__(self, external_id, data=None):
        super().__init__(data or {})
        self.external_id = external_id
        self._pipelines = None

    @property
    @abstractmethod
    def pipelines(self):
        """List of Pipelines objects inside this project.

        Each backend should implement this property.
        """
        pass

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def description(self):
        pass

    @property
    @abstractmethod
    def repository_url(self):
        pass

    def as_dict(self):
        return {'external_id': self.external_id,
                'name': self.name,
                'description': self.description,
                'repository_url': self.repository_url}

    @cached_property
    def jobs_as_dict(self):
        jobs = []
        for pipeline in self.pipelines:
            for job in pipeline.jobs:
                jobs.append(job.as_dict())
        return jobs

    @cached_property
    def tests_as_dict(self):
        tests = []
        for pipeline in self.pipelines:
            for job in pipeline.jobs:
                for test in job.tests:
                    tests.append(test.as_dict())
        return tests


class GenericPipeline(GenericObj):
    def __init__(self, data, project=None):
        super().__init__(data)
        self.project = project
        self._jobs = None

    @property
    @abstractmethod
    def external_id(self):
        pass

    @property
    @abstractmethod
    def status(self):
        pass

    @property
    @abstractmethod
    def sha(self):
        pass

    @property
    @abstractmethod
    def ref(self):
        pass

    @property
    @abstractmethod
    def source(self):
        pass

    @property
    @abstractmethod
    def created_at(self):
        pass

    @property
    @abstractmethod
    def updated_at(self):
        pass

    @property
    @abstractmethod
    def external_url(self):
        pass

    @property
    @abstractmethod
    def jobs(self):
        """List of Jobs objects inside a pipeline.

        Each backend should implement this property.
        """
        pass

    def as_dict(self):
        return {'external_id': self.external_id,
                'status': self.status,
                'sha': self.sha,
                'ref': self.ref,
                'source': self.source,
                'created_at': self.created_at,
                'updated_at': self.updated_at,
                'external_url': self.external_url}


class GenericJob(GenericObj):
    def __init__(self, data, runner=None, pipeline=None):
        super().__init__(data)
        self.runner = runner
        self.pipeline = pipeline

    @property
    @abstractmethod
    def external_id(self):
        pass

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def status(self):
        pass

    @property
    @abstractmethod
    def stage(self):
        pass

    @property
    @abstractmethod
    def external_url(self):
        pass

    @property
    @abstractmethod
    def created_at(self):
        pass

    @property
    @abstractmethod
    def started_at(self):
        pass

    @property
    @abstractmethod
    def finished_at(self):
        pass

    @property
    @abstractmethod
    def test_results(self):
        """List of TestResult objects inside a Job.

        Each backend should implement this property.
        """
        pass

    def as_dict(self):
        return {'external_id': self.external_id,
                'name': self.name,
                'status': self.status,
                'stage': self.stage,
                'external_url': self.external_url,
                'created_at': self.created_at,
                'started_at': self.started_at,
                'finished_at': self.finished_at,
                'runner': self.runner.as_dict() if self.runner else None,
                'test_results': [tr.as_dict() for tr in self.test_results]}


class GenericRunner(GenericObj):
    @property
    @abstractmethod
    def external_id(self):
        pass

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def description(self):
        pass

    @property
    @abstractmethod
    def owner(self):
        pass

    @property
    @abstractmethod
    def architecture(self):
        pass

    @property
    @abstractmethod
    def platform(self):
        pass

    def as_dict(self):
        return {'external_id': self.external_id,
                'name': self.name,
                'description': self.description,
                'owner': self.owner,
                'architecture': self.architecture,
                'platform': self.platform}


class GenericTestResult(GenericObj):
    def __init__(self, data, job=None):
        super().__init__(data)
        self.job = job
        self.__id = str(uuid4())

    @property
    def external_id(self):
        return self.__id

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def class_name(self):
        pass

    @property
    @abstractmethod
    def status(self):
        pass

    @property
    @abstractmethod
    def execution_time(self):
        pass

    def as_dict(self):
        return {'external_id': self.external_id,
                'name': self.name,
                'class_name': self.class_name,
                'status': self.status,
                'execution_time': self.execution_time}
