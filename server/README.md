# Insights4CI - Server side

This directory is holding a FastAPI project with some basic REST API. The 
server component will handle only the API requests, leaving the frontend views
to VueJS (under the directory `ui`).

Keep in mind that this is a project in the initial stage of development; there
is no authentication/authorization. It would be best not to expose this API or
the database outside your trusted network.

## Install requirements

    $ pip3 install -r requirements.txt

## Configure the database

Make sure that you have a MariaDB server running with the proper credentials:

    database: insights4ci
    user: insigths4ci
    password: insights4ci

## Starting the server

While this is not a proper Python package, you need to point your `PYTHONPATH`
to the right location:

    $ export PYTHONPATH=./src:$PYTHONPATH

And, start the Uvicorn ASGI server:

    $ uvicorn insights4ci.main:app --reload

## Accessing the docs

FastAPI it will be listening on port 8000, and you can access the documentation
in two different views:

 * http://localhost:8000/docs
 * http://localhost:8000/redoc

The first one you have the possibility to call the methods directly from the
browser.
