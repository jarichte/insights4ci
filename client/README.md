# Insights4CI - REST API Client

This directory is holding a rudimentary Python library for the insights4ci REST
API.

:warning: Since Insights4CI API is in constant development, this is the best
way to consume the API. We will keep this client in sync with the API.


## Install requirements

    $ pip3 install -r requirements.txt

## Using it

While this is not a proper Python package, you need to point your `PYTHONPATH`
to the right location:

    $ export PYTHONPATH=./src:$PYTHONPATH


Now, you can import and consume the library:


```python
from insights4ci.client.api import Insights4CIClient, Project                                                                                                                      

client = Insights4CIClient(url="http://localhost:8000")                                                                                                                            
for project in Project.get_all(client): 
	print(project.data)
``` 
