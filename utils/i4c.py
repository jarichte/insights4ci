import sys
import os
import requests
import logging
import logging.config

from argparse import ArgumentParser
from insights4ci.backends import gitlab

from insights4ci.client.api import Insights4CIClient, Project, Pipeline, Job

SUPPORTED_BACKENDS = {'gitlab': gitlab}

DEFAULT_I4C_ADDRESS = "http://localhost:8000"

logging.config.fileConfig('logging.conf')
LOG = logging.getLogger('insights4ci.client.cli')


def register_project(args):
    """Submit a new project.

    This should be executed only by admins on approved projects and only once
    otherwise an exception it will be raised telling this project is already
    registered.
    """

    data = vars(args)
    data.pop('func')

    client = Insights4CIClient(os.environ.get('I4C_ADDRESS',
                                              DEFAULT_I4C_ADDRESS))
    try:
        project = Project(data).post(client)
    except requests.exceptions.HTTPError as ex:
        LOG.error(str(ex))
        LOG.error("Failed to register the project. Exiting...")
        return 1

    LOG.info(f"Project {data.get('name')} created:")
    for key, value in project.as_dict().items():
        LOG.info(f"{key}: {value}")


def import_jobs(args):
    try:
        backend = SUPPORTED_BACKENDS[args.backend]
    except KeyError:
        msg = ("Backend not supported. For now, only 'gitlab' "
               "is supported.")
        LOG.error(msg)

    LOG.info(f"Fetching project ({args.external_id}) from {args.backend}...")
    g_project = backend.Project(args.external_id)

    client = Insights4CIClient(os.environ.get('I4C_ADDRESS',
                                              DEFAULT_I4C_ADDRESS))
    try:
        project = Project.get_by_id(client, args.internal_id)
    # TODO: client api should raise its own Exception
    except requests.exceptions.HTTPError as ex:
        LOG.error(str(ex))
        LOG.error("Failed to get project from Insights4CI! Exiting...")
        return 1

    for g_pipeline in g_project.pipelines:
        # Creates a local pipeline object and submmit it.
        pipeline = Pipeline(project, g_pipeline.as_dict()).post(client)
        for g_job in g_pipeline.jobs:
            # Creates a local job object and submmit it.
            # A job here, has already its runner and test results.
            Job(pipeline, g_job.as_dict()).post(client)


def main(argv=sys.argv[1:]):
    epilog = ("This CLI is in beta stage and available just for basic "
              "operations. Please use it as reference for your own use "
              "case or visit the REST API.")
    parser = ArgumentParser(description="insights4ci basic CLI.",
                            epilog=epilog)

    subparsers = parser.add_subparsers(help='sub-command help')

    # projects-register
    help_msg = ("Register a new project.")
    projects = subparsers.add_parser("projects-register",
                                     help=help_msg)
    projects.add_argument('--name',
                          required=True,
                          help="Project's name.")
    projects.add_argument('--description',
                          help="Project's description.")
    projects.add_argument('--repository-url',
                          help="Source code repository.")
    projects.set_defaults(func=register_project)

    # jobs-import
    help_msg = ("Import project data from external backend. This will fetch "
                "all jobs data found. The 'source' field for each pipeline "
                "found it will be properly set depending on the backend used")
    jobs = subparsers.add_parser("jobs-import",
                                 help=help_msg)
    jobs.add_argument('--external_id',
                      required=True,
                      type=str,
                      help="Project's external id on that backend.")
    jobs.add_argument('--internal_id',
                      required=True,
                      type=str,
                      help="Project's internal id on Insights4CI.")
    jobs.add_argument('--backend',
                      default="gitlab",
                      choices=SUPPORTED_BACKENDS.keys(),
                      help="Fetching data from backend. Default: gitlab.")
    jobs.set_defaults(func=import_jobs)

    args = parser.parse_args(argv)
    return args.func(args)


if __name__ == "__main__":
    sys.exit(main())
