#!/usr/bin/env python3
import gitlab
import json

from uuid import uuid4


PROJECT_ID = '11167699'
GITLAB_ENDPOINT = 'https://gitlab.com'


class Project:
    def __init__(self, project_id):
        self.project_id = project_id
        self._gl = gitlab.Gitlab(GITLAB_ENDPOINT)
        print("Connected...")
        # Cache objects
        self._project = None

    @property
    def project(self):
        print("Project...")
        if not self._project:
            self._project = self._gl.projects.get(self.project_id)
        return self._project

    @property
    def pipelines(self):
        # as_list=False to get a generator
        print("Pipelines...")
        return [Pipeline(pipeline.attributes, self)
                for pipeline in self.project.pipelines.list()]

    def get_pipeline(self, pipeline_id):
        full_pipeline = self.project.pipelines.get(pipeline_id)
        return Pipeline(full_pipeline.attributes, self)

    @property
    def jobs(self):
        jobs = []
        for pipeline in self.pipelines:
            # Skip running pipelines
            if pipeline.status in ['running', 'skipped', 'canceled']:
                continue
            for job in pipeline.jobs:
                jobs.append(job.as_dict())
        return jobs

    @property
    def tests(self):
        tests = []
        for pipeline in self.pipelines:
            # Skip running pipelines
            if pipeline.status in ['running', 'skipped', 'canceled']:
                continue

            print(pipeline.id, pipeline.status)
            print(pipeline.data)

            for job in pipeline.jobs:
                print(job.data)
                for test in job.tests:
                    tests.append(test.as_dict())
            print("... ", len(pipeline.jobs))
        return tests

    @property
    def checkouts(self):
        """Experimental: fake mapping checkouts/pipelines."""
        return [pipeline.as_checkout() for pipeline in self.pipelines]

    @property
    def builds(self):
        """Experimental: fake mapping builds/jobs."""
        return [job.as_build() for job in self.jobs]

    def as_kcidb_model(self):
        return {'id': 42,
                'path_with_namespace': 'qemu-project/qemu',
                'instance_url': 'https://gitlab.com'}


class GenericObj:
    def __init__(self, data):
        self.data = data
        self.to_print = []

    def __hash__(self):
        return hash(self.as_json())

    def as_dict(self):
        return {key: value for key, value in self.data.items()
                if key in self.to_print}

    def as_json(self, sort_keys=True, indent=4):
        return json.dumps(self.data, sort_keys=sort_keys, indent=indent)


class Pipeline(GenericObj):
    def __init__(self, data, project=None):
        super().__init__(data)
        self.project = project
        self.__raw_pipeline = None
        self.__jobs = None
        self.__report = None

    @property
    def _raw_pipeline(self):
        if self.__raw_pipeline is None:
            self.__raw_pipeline = self.project.project.pipelines.get(self.id)
        return self.__raw_pipeline

    @property
    def variables(self):
        return {}

    @property
    def report(self):
        if self.__report is None:
            self.__report = self._raw_pipeline.test_report.get()
        return self.__report

    @property
    def id(self):
        return self.data.get('id')

    @property
    def status(self):
        return self.data.get('status')

    @property
    def jobs(self):
        if self.__jobs is None:
            self.__jobs = [Job(job.attributes, self)
                           for job in self._raw_pipeline.jobs.list()]
        return self.__jobs

    def as_kcidb_model(self):
        """Some pipelines will have less attributes, based on the status."""
        return {'id': self.id,
                'status': self.status,
                'started_at': self.data.get('started_at'),
                'created_at': self.data.get('created_at'),
                'finished_at': self.data.get('finished_at'),
                'ref': self.data.get('ref'),
                'sha': self.data.get('sha'),
                'project': self.project.as_kcidb_model(),
                'variables': self.variables,
                'duration': self.data.get('duration')}


class Job(GenericObj):
    def __init__(self, data, pipeline=None):
        super().__init__(data)
        self.pipeline = pipeline
        self.to_print = ['id', 'pipeline_id', 'project_id', 'status', 'stage', 'name', 'ref', 'allow_failure', 'started_at', 'created_at', 'finished_at', 'runner', 'web_url', 'pipeline', 'commit']
        self.__tests = None

    @property
    def id(self):
        return self.data.get('id')

    @property
    def name(self):
        return self.data.get('name')

    @property
    def started_at(self):
        return self.data.get('started_at')

    @property
    def environment(self):
        return {'comment': self.data['runner']['description']}

    @property
    def tests(self):
        if self.__tests is None:
            for suite in self.pipeline.report.test_suites:
                if suite['name'] == self.name:
                    self.__tests = [Test(test_case, self)
                                    for test_case in suite['test_cases']]
        return self.__tests or []

    def as_dict(self):
        try:
            runner = self.data.get('runner').get('name')
        except (KeyError, AttributeError):
            runner = None

        # from pprint import pprint
        # pprint(self.data)

        return {'id': self.data.get('id'),
                'pipeline': self.data.get('pipeline_id'),
                'project': self.data.get('project_id'),
                'runner': runner,
                'status': self.data.get('status'),
                'stage': self.data.get('stage'),
                'name': self.data.get('name'),
                'ref': self.data.get('ref'),
                'allow_failure': self.data.get('allow_failure'),
                'created_at': self.data.get('created_at'),
                'started_at': self.data.get('started_at'),
                'finished_at': self.data.get('finished_at'),
                'commit': self.data.get('commit', {}).get('id')}


class Test(GenericObj):
    def __init__(self, data, job=None):
        super().__init__(data)
        self.job = job
        self.to_print = ['status', 'name', 'classname', 'execution_time']
        self.__id = str(uuid4())

    @property
    def start_time(self):
        return "2021-11-05T14:14:38.140261+00:00"

    @property
    def id(self):
        return self.__id

    def as_dict(self):
        result = super().as_dict()
        result['job'] = self.job.id
        # result['job_started_at'] = self.job.started_at
        return result


if __name__ == "__main__":
    p = Project(PROJECT_ID)
    data = {"jobs": {job['id']: job for job in p.jobs},
            "tests": p.tests}
    with open('./test.json', 'a') as fp:
        json.dump(data, fp, indent=4)
