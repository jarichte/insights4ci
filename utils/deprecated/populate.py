#!/usr/bin/env python3
"""Basic (and ugly) Gitlab import script.

For now it is just getting data from qemu-project/qemu.
"""

from copy import copy
from datetime import datetime

from insights4ci.utils.glimport import Project as GLProject
from insights4ci.database import SessionLocal
from insights4ci.models import Project, Job, Pipeline, Test, TestResult, Runner


PROJECT_ID = 1


def create_projects(db):
    projects = [{'name': 'qemu',
                 'gitlab_url': 'http://gitlab.com/qemu-project/qemu'},
                {'name': 'libvirt',
                 'gitlab_url': 'http://gitlab.com/libvirt/libvirt'}]

    for data in projects:
        project = Project.create_from_dict(db, data)
        print("Created ", project.name)


def create_test_results(db):
    for pipeline in GLProject('11167699').pipelines:
        data = copy(pipeline.data)
        data['external_id'] = data.pop('id')
        db_pipeline = Pipeline.create_from_dict(db,
                                                data,
                                                PROJECT_ID)
        for job in pipeline.jobs:
            data = copy(job.data)
            if data['runner']:
                runner_db = Runner.get_by_name_or_create(db,
                                                         data['name'],
                                                         PROJECT_ID)
                runner_db.description = data['runner']['description']
                runner_db.external_id = data['runner']['id']
                runner_db.last_seen = datetime.strptime(data['started_at'][:19],
                                                        '%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
                runner_db.save_to_session(db)

            data['external_id'] = data.pop('id')
            data['commit'] = data.pop('commit').get('id', 'foo')
            print(data)
            try:
                data['runner'] = data.pop('runner').get('description', 'foo')
            except AttributeError:
                data['runner'] = 'foo'

            job_db = Job.create_from_dict(db,
                                          data,
                                          db_pipeline.id)
            print("Job Created: ", job_db.id, job_db.external_id)

            for test in job.tests:
                data = copy(test.data)
                test_db = Test.get_by_name_or_create(db,
                                                     data['name'],
                                                     data['classname'],
                                                     PROJECT_ID)
                TestResult.create_from_dict(db, data, test_db.id, job_db.id)


if __name__ == "__main__":
    db = SessionLocal()
    create_projects(db)
    create_test_results(db)
    db.close()
